import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faAndroid,
  fab,
  faFacebookSquare,
  faTwitterSquare,
} from "@fortawesome/free-brands-svg-icons";
import {
  faCartShopping,
  faStar,
} from "@fortawesome/free-solid-svg-icons";

import {
  faHeart,
  faStar as farStar,
  faUser,
} from "@fortawesome/free-regular-svg-icons";

library.add(
  fab,
  faFacebookSquare,
  faTwitterSquare,
  faStar,
  farStar,
  faHeart,
  faCartShopping,
  faUser
);
