import RouteGuard from "../components/RouteGuard/RouteGuard";
import { wrapper } from "../redux";
import '../styles/globals.css';
import '../icons';
import { Auth0Provider } from "@auth0/auth0-react";

function MyApp({ Component, pageProps }) {
  return (
    // <RouteGuard>
      <Auth0Provider
        domain="dev-x5soe2vw.us.auth0.com"
        clientId="r9zkVFKcr6HYFN90uUA3CrY7k1HWmLea"
        redirectUri={typeof window != 'undefined' && window.location.origin}
      >
        <Component {...pageProps} />
      </Auth0Provider>
    // </RouteGuard>
  )
}

export default wrapper.withRedux(MyApp)