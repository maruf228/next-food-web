import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { logout } from '../../redux/actions/authActions';
import { useAuth0 } from "@auth0/auth0-react";

export default function RouteGuard({ children }) {
  const { user, isAuthenticated, isLoading } = useAuth0();

  const dispatch = useDispatch();
  const router = useRouter();
  const [authorized, setAuthorized] = useState(false);
  const auth = useSelector((state) => state.authentication);

  const handleLogout = async () => {
    dispatch(logout());
    setAuthorized(false);
    router.push({
      pathname: '/login',
      query: { returnUrl: router.asPath }
    });
  }

  const authCheck = (url) => {
    const publicPaths = ['/','/login', '/logout'];
    const path = url.split('?')[0];
    
    if (!localStorage.getItem("access_token") && !publicPaths.includes(path)) {
      handleLogout();
    } else if (localStorage.getItem("access_token")) {
      const decodedJwt = JSON.parse(atob(localStorage.getItem("access_token").split('.')[1]));
      const jwtExpired = decodedJwt.exp * 1000 < Date.now();
      if (jwtExpired && !publicPaths.includes(path)) {
        handleLogout(url);
      } else {
        setAuthorized(true);
      }
    } else {
      setAuthorized(true);
    }
  }

  useEffect(() => {
    console.log('is authenticated? ', isAuthenticated)
    // on initial load - run auth check
    authCheck(router.asPath);

    // on route change start - hide page content by setting authorized to false
    const hideContent = () => setAuthorized(false);
    router.events.on('routeChangeStart', hideContent);

    // on route change complete - run auth check
    router.events.on('routeChangeComplete', authCheck)

    // unsubscribe from events in useEffect return function
    return () => {
      router.events.off('routeChangeStart', hideContent);
      router.events.off('routeChangeComplete', authCheck);
    }
  }, []);

  return (authorized && children);
}
