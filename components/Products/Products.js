import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './products.module.css';
import StarGenerator from '../utils/star-generator/starGenerator';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';

export default function Products() {
  const [isLoading, setIsLoading] = useState(false);
  const [productList, setProductList] = useState([]);
  const [count, setCount] = useState(1);

  const getProductList = async () => {
    try {
      let response = await axios.get('https://raw.githubusercontent.com/aslamhossain-selise/example-productlist/main/productlist.json');

      if (response?.data?.products && response?.data?.products.length > 0) {
        setProductList(response?.data?.products)
      }
    } catch (error) {
      console.log('error', error)
    }
  }
  useEffect(() => {
    getProductList();
  }, []);
  return (
    <>
      {isLoading ?
        <div>Loading...</div>
        :
        <div className={styles.productListWrapper}>
        {productList.map((item, index) => {
          if (index < 4) {
            return (
              <div key={index} className={styles.itemWrapper}>
                <div className={styles.priceWrapper}>
                  $ <span className={styles.bigPrice}>{item.price}</span>
                </div>
                <div className={styles.title}>{item.title}</div>

                <StarGenerator count={5} />
  
                <div className={styles.quantityWrapper}>
                  Quantity: 
                  <span>
                    <button className={styles.plusMinusBtn}>-</button>
  
                    {count}
  
                    <button className={styles.plusMinusBtn}>+</button>
                  </span>
                </div>
  
                <div>
                  <button className={styles.shopNowBtn}>Shop Now</button>
                </div>
  
                <div className={styles.foodImageWrapper}>
                  <Image
                    src={item.image}
                    width={124}
                    height={124}
                    className={styles.foodImage}
                  />
                </div>
                  
              </div>
            )
          }
        })}
      
        </div>
      }
    </>
    
  )
}