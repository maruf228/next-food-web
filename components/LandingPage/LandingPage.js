import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Products from '../Products/Products';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './landingPage.module.css';

export default function LandingPage() {
  const router = useRouter();
  return (
    <div className={styles.container}>
      <nav>
        <div className={styles.navWrapper}>
          <div className={styles.logoText}>
            LOGO HERE
          </div>

          <div className={styles.navItemsWrapper}>
            <Link href="/" className={styles.link}>
              <a className={router.pathname == "/" ? "active" : ""}>Home</a>
            </Link>

            <Link href="/services" className={styles.link}>
              <a className={router.pathname == "/services" ? "active" : ""}>Services</a>
            </Link>

            <Link href="/hotline" className={styles.link}>
              <a className={router.pathname == "/hotline" ? "active" : ""}>Hotline</a>
            </Link>

            <Link href="/categories" className={styles.link}>
              <a className={router.pathname == "/categories" ? "active" : ""}>Categories</a>
            </Link>

            <Link href="/about" className={styles.link}>
              <a className={router.pathname == "/about" ? "active" : ""}>About</a>
            </Link>
          </div>

          <div className={styles.bannerWrapper}>
            <div className={styles.iconsWrapper}>
              <FontAwesomeIcon
                icon={["far", "heart"]}
                transform="shrink-2"
                className={styles.icon}
              />

              <FontAwesomeIcon
                icon={"cart-shopping"}
                transform="shrink-2"
                className={styles.icon}
              />

              <FontAwesomeIcon
                icon={["far", "user"]}
                transform="shrink-2"
                className={styles.icon}
              />
            </div>
            
            <div className={styles.bigImageWrapper}>
              <Image
                src="/landingBigImage.png"
                width="400"
                height="400"
                layout='fixed'
                className={styles.bigImage}
              />
            </div>
              
          </div>
        </div>
      </nav>

      <div className={styles.contentWrapper}>
        <div className={styles.motoText}>
          <div>
            Your <span className={styles.motoLightRedText}>Favorite food</span>
          </div>

          <div>
            delivered <span className={styles.motoLightRedText}>hot</span> and
          </div>

          <div>fresh</div>
        </div>

        <div className={styles.siteDescription}>
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>

        <div className={styles.orderAndWatchMoreSection}>
          <div>
            <button className={styles.orderNowBtn}>Order Now</button>
          </div>

          <div className={styles.crossText}>$30</div>

          <div className={styles.updatedPrice}>$ <span className={styles.bigUpdatedPrice}>20</span></div>

          <div className={styles.watchMoreWrapper}>
            <Image
              src="/watchMore.png"
              width="25"
              height="25"
              layout='fixed'
              className={styles.watchMoreIcon}
            />
             
            <span>Watch More</span>
          </div>
        </div>

        <Products />
      </div>
    </div>
  )
}