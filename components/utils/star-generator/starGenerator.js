import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './stargenerator.module.css';

export default function StarGenerator(props) {
  const [nonStar, setNonStar] = useState(5 - props.count);
  const [star, setStar] = useState(props.count);
  
  return (
    <div className={styles.wrapper}>
      {Array(star).fill(1).map((el, i) =>
        <FontAwesomeIcon
          icon="star"
          transform="shrink-2"
          className={styles.icon}
        />
      )}

      {Array(nonStar).fill(1).map((el, i) =>
        <FontAwesomeIcon
        icon={["far", "star"]}
          transform="shrink-2"
          className={styles.icon}
        />
      )}
    </div>

  )
}
