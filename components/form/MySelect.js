import React, { useState, useEffect } from 'react';
import Select from "react-select"

const overrideStrings = {
  allItemsAreSelected: "All Selected",
  clearSearch: "Clear Search",
  noOptions: "No options",
  search: "Search",
  selectAll: "Select All",
  selectSomeItems: "Select...",
};

export default function MySelect(props) {
  const [data, setData] = useState(props.data);
  const [selectedData, setSelectedData] = useState();

  return (
    <Select
      overrideStrings={overrideStrings}
      options={data}
      value={selectedData}
      onChange={(val) => {setSelectedData(val)}}
      labelledBy="Select Route & SR"
      isClearable={true}
      // styles={customStyles}
    />
  )
}