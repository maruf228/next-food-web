import axios from "axios";
import { API } from "../backend";

export const httpClient = async () => {
  const config = axios.create({
    baseURL: API,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    },
  });

  return config;
};
