import * as api from '../api/auth';

export const login = (user) => async (dispatch) => {
  try {
    return api.loginUser(user)
      .then(({data}) => {
        if (data.success === true) {
          dispatch({ type: 'LOGIN_SUCCESS', payload: data });
          return data;
        }
      })
      .catch(err => {
        dispatch({ type: 'LOGIN_FAIL' });
        return err
      })
  } catch (error) {
    console.log(error);
  }
}

export const logout = () => async (dispatch) => {
  try {
    let data = await dispatch({ type: 'LOGOUT_SUCCESS' });
  } catch (error) {
    console.log(error);
  }
}
