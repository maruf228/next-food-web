// import * as api from '../api/projects';

export const SbuSet = (selectedSBU) => async(dispatch) => {
  try {
    await dispatch({ type: 'SET_SELECTED_SBU', payload: selectedSBU });
  } catch (error) {
    console.log(error.message);
  }
}
