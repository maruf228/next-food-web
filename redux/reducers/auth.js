export default (
  auth = {
    access_token:
      typeof window !== "undefined" && localStorage.getItem("access_token"),
    isAuthenticated: (typeof window !== "undefined" && localStorage.getItem("access_token")) ? true : false,
    isLoading: null,
  },
  action
) => {
  switch (action.type) {
    case "USER_LOADING":
      return {
        ...auth,
        isLoading: true,
      };
    case "USER_LOADED":
      return {
        ...auth,
        isAuthenticated: true,
        isLoading: false,
      };
    case "LOGIN_SUCCESS":
      localStorage.setItem("access_token", action?.payload?.access_token);

      return {
        ...auth,
        access_token: action.payload?.access_token,
        isAuthenticated: true,
        isLoading: false,
      };
    case "AUTH_ERROR":
    case "LOGIN_FAIL":
    case "LOGOUT_SUCCESS":
    case "REGISTER_FAIL":
      localStorage.removeItem("access_token");

      return {
        ...auth,
        access_token: null,
        isAuthenticated: false,
        isLoading: false,
      };
    default:
      return auth;
  }
};
