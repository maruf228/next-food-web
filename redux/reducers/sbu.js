export default (sbu = {
  selected_sbu: null
}, action) => {
  switch (action.type) {
    case 'SET_SELECTED_SBU':
      return {
        selected_sbu: action.payload
      };
    default:
      return sbu;
  }
}
