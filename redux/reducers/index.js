import { combineReducers } from "redux";
import auth from "./auth";
import sbu from './sbu';

const rootReducer = combineReducers({
  authentication: auth,
});

export default rootReducer;

