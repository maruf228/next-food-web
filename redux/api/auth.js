import axios from 'axios';
import {API} from '../../backend';

const url = `${process.env.NEXT_PUBLIC_BACKEND}`;


// login to get token
export const loginUser = (user) => axios.post(`${url}/auth/login`, user);

// get user info according to token
// export const fetchUser = (config) => axios.get(`${url}/auth/user`, config);

// create a user
// export const createUser = (user) => axios.post(`${url}/users`, user);

